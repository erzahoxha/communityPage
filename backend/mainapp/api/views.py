from rest_framework.generics import ListAPIView, RetrieveAPIView

from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from django.contrib import auth, messages

from mainapp.models import Blogs, Kategorite, Challenges, Comments_challenges, Comments_blogs, RegisterForm
from .serializers import BlogsSerializer, KategoriteSerializer, ChallengesSerializer, Comments_challengesSerializer, Comments_blogsSerializer

class BlogsListView(ListAPIView):
    queryset = Blogs.objects.all()
    serializer_class = BlogsSerializer

class BlogsDetailView(RetrieveAPIView):
    queryset = Blogs.objects.all()
    serializer_class = BlogsSerializer


class KategoriteListView(ListAPIView):
    queryset = Kategorite.objects.all()
    serializer_class = KategoriteSerializer

class KategoriteDetailView(RetrieveAPIView):
    queryset = Kategorite.objects.all()
    serializer_class = KategoriteSerializer


class ChallengesListView(ListAPIView):
    queryset = Challenges.objects.all()
    serializer_class = ChallengesSerializer

class ChallengesDetailView(RetrieveAPIView):
    queryset = Challenges.objects.all()
    serializer_class = ChallengesSerializer


class Comments_challengesListView(ListAPIView):
    queryset = Comments_challenges.objects.all()
    serializer_class = Comments_challengesSerializer

class Comments_challengesDetailView(RetrieveAPIView):
    queryset = Comments_challenges.objects.all()
    serializer_class = Comments_challengesSerializer


class Comments_blogsListView(ListAPIView):
    queryset = Comments_blogs.objects.all()
    serializer_class = Comments_blogsSerializer

class Comments_blogsDetailView(RetrieveAPIView):
    queryset = Comments_blogs.objects.all()
    serializer_class = Comments_blogsSerializer


def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            '''
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            '''
            return redirect('home')
    else:
        form = UserCreationForm()
    return render(request, 'register.html', {'form': form}) 


def login(request):
    if request.user.is_authenticated():
        return redirect('admin_page')
 
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = auth.authenticate(username=username, password=password)
 
        if user is not None:
            # correct username and password login the user
            auth.login(request, user)
            return redirect('admin_page')
 
        else:
            messages.error(request, 'Error wrong username/password')
 
    return render(request, 'blog/login.html')
 
 
def logout(request):
    auth.logout(request)
    return render(request,'blog/logout.html')

